import React from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import {Panel, PanelHeader, Epic, Tabbar, TabbarItem , View
,FormLayoutGroup,FormLayout,Select,Input,Button,List,Cell, Title,Group,Header,SimpleCell,Avatar} from '@vkontakte/vkui'
import Icon24Filter from '@vkontakte/icons/dist/24/filter';
import Icon24FavoriteOutline from '@vkontakte/icons/dist/24/favorite_outline';
import Icon16Advertising from '@vkontakte/icons/dist/16/advertising';
import Icon16Users from '@vkontakte/icons/dist/16/users';
//bridge.send("VKWebAppInit");
class Converter extends React.Component {
   static get propTypes () {
        return {
            history: PropTypes.array
        }
}
    constructor(props) {
        super(props);
        this.state = {
            activeStory: 'converter',
            valute: {},
            result:1,
            firstSelector: 'RUB',
            secondSelector:''
        }
    }

    componentDidMount() {
        axios.get("https://api.exchangerate-api.com/v4/latest/RUB").then((res) => {
            this.setState({
                valute: res.data.rates
            })
        })
    }
    inValie () {


    }


    onStoryChange (e) {

        this.setState({ activeStory: e.currentTarget.dataset.story })


    }


    render() {
        return (
            <Epic activeStory={this.state.activeStory} tabbar={
                <Tabbar>
                    <TabbarItem
                        onClick={(e) => this.onStoryChange(e)}
                        selected={this.state.activeStory === 'converter'}
                        data-story="converter"
                        text="Конвертер"
                    ><Icon24Filter/></TabbarItem>
                    <TabbarItem
                        onClick={(e) => this.onStoryChange(e)}
                        selected={this.state.activeStory === 'profile'}
                        data-story="profile"
                        text="Об авторе"
                    ><Icon24FavoriteOutline/></TabbarItem>
                </Tabbar>
            }>
                <View id="converter" activePanel="converter">
                    <Panel id="converter">
                        <PanelHeader>Конвертер</PanelHeader>
                        <FormLayoutGroup>
                            <FormLayout>
                                <Select value={this.state.firstSelector} top="Конвертировать из" placeholder="Выберите валюту">
                                    {Object.keys(this.state.valute).map(valute => (
                                        <option key={valute} value={valute}>
                                            {valute}
                                        </option>
                                    ))}
                                </Select>
                            </FormLayout>
                            <FormLayout>
                                <Input top="Колличество" type="number" min={0}/>
                            </FormLayout>
                        </FormLayoutGroup>
                        <FormLayoutGroup>
                            <FormLayout>
                                <Select value={this.state.secondSelector} top="Конвертировать в" placeholder="Выберите валюту">
                                    {Object.keys(this.state.valute).map(valute => (
                                        <option key={valute} value={valute}>
                                            {valute}
                                        </option>
                                    ))}
                                </Select>
                            </FormLayout>
                            <FormLayout>
                                <Input top="Результат" type="number" min={0} />
                            </FormLayout>
                        </FormLayoutGroup>

                        <FormLayout>
                            <Button size="xl">
                                Сохранить
                            </Button>
                        </FormLayout>
                        <FormLayout>
                            <List>
                                {this.props.history.map((convert) => {
                                    return(
                                        <Cell>{convert}</Cell>
                                    )
                                })

                                }
                            </List>
                        </FormLayout>
                    </Panel>
                </View>
                <View id="profile" activePanel="profile">
                    <Panel id="profile">
                        <PanelHeader>Об авторе</PanelHeader>
                        <Group>
                            <Header mode="secondary"></Header>
                            <SimpleCell
                                description="VKontakte"
                                before={<Avatar src={"https://sun9-48.userapi.com/9wA1XiHCRJ0rNGe-cxcdaGjZvsF4CMVVOnrrvA/5r_R3JObqq8.jpg"}/>}
                            >
                                Эйрих Елизавета
                            </SimpleCell>
                        </Group>
                        <FormLayout>
                            <Title level="3" weight="regular" style={{ marginBottom: 16 }}> Привет, я автор этой страшной штуки. Пока это первый мой первый проэкт вк, но каждый же с чего то начинал. Пусть это криво и где то не работает, но оно моё. И когда я гляну на него в будущем,только тогда будет понятно, какую работу я проделала, чтобы сделать что-то большее чем этот проэкт</Title>
                        </FormLayout>
                        <FormLayout>
                            <Button before={<Icon16Users/> }onClick={() => bridge.send("VKWebAppJoinGroup", {"group_id": 199159884}) }  >
                                Подписаться на сообщество
                            </Button>
                        </FormLayout>
                        <FormLayout>
                            <Button before={<Icon16Advertising/>} onClick={() => bridge.send("VKWebAppShowWallPostBox", {"message": "Здраствуйте"})} >
                               Репост
                            </Button>

                        </FormLayout>
                    </Panel>
                </View>
            </Epic>

        )
    }
}

export default Converter;